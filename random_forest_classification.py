# Random Forest Classification

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('Trainingset.csv', usecols=['Page/File', 'Referrers', 'ID'])
testset = pd.read_csv('Testset.csv', usecols=['Page/File', 'Referrers', 'ID'])

#Label Encoding
from sklearn.preprocessing import LabelEncoder
labelEncoder = LabelEncoder()
dataset['Page/File']= labelEncoder.fit_transform(dataset['Page/File'])
dataset['Referrers'] = labelEncoder.fit_transform(dataset['Referrers'])
testset['Page/File'] = labelEncoder.fit_transform(testset['Page/File'])
testset['Referrers'] = labelEncoder.fit_transform(testset['Referrers'])

X = dataset.iloc[:,:].values
y = dataset.iloc[:, 1].values

X_train = X
y_train = y
X_test = testset.iloc[:,:].values
y_test = testset.iloc[:,1].values

# Splitting the dataset into the Training set and Test set
#from sklearn.model_selection import train_test_split
#X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state = 0)

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

# Training the Random Forest Classification model on the Training set
from sklearn.ensemble import RandomForestClassifier
classifier = RandomForestClassifier(n_estimators = 20, criterion = 'entropy', random_state = 0)
classifier.fit(X_train, y_train)

# Predicting the Test set results
y_pred = classifier.predict(X_test)

#Inverse name mapping:
dataset = pd.read_csv('Trainingset.csv')
dataset['Page/File Encoded']= labelEncoder.fit_transform(dataset['Page/File'])
dataset['Referrers Encoded'] = labelEncoder.fit_transform(dataset['Referrers'])
ref_dict = {}
for i in dataset.index:
    code = dataset['Referrers Encoded'][i]
    name = dataset['Referrers'][i]
    ref_dict[code] = name
    print(code)
y_pred_inverse = {}
for idx, val in np.ndenumerate(y_pred):
    for key in ref_dict:
        if key == val:
            y_pred_inverse[idx[0]]=  ref_dict[key]

dataset = pd.read_csv('Testset.csv')
for i in dataset.index:
    x = y_pred_inverse[i]
    print(x)
    dataset["Referrers"][i] = y_pred_inverse[i]
    
dataset.to_csv(path_or_buf = "RFC_Prediction.csv")
"""
# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
print(cm)

# Visualising the Training set results
from matplotlib.colors import ListedColormap
X_set, y_set = X_train, y_train
X1, X2 = np.meshgrid(np.arange(start = X_set[:, 0].min() - 1, stop = X_set[:, 0].max() + 1, step = 0.01),
                     np.arange(start = X_set[:, 1].min() - 1, stop = X_set[:, 1].max() + 1, step = 0.01))
plt.contourf(X1, X2, classifier.predict(np.array([X1.ravel(), X2.ravel()]).T).reshape(X1.shape),
             alpha = 0.75, cmap = ListedColormap(('red', 'green')))
plt.xlim(X1.min(), X1.max())
plt.ylim(X2.min(), X2.max())
for i, j in enumerate(np.unique(y_set)):
    plt.scatter(X_set[y_set == j, 0], X_set[y_set == j, 1],
                c = ListedColormap(('red', 'green'))(i), label = j)
plt.title('Random Forest Classification (Training set)')
plt.xlabel('Age')
plt.ylabel('Estimated Salary')
plt.legend()
plt.show()

# Visualising the Test set results
from matplotlib.colors import ListedColormap
X_set, y_set = X_test, y_test
X1, X2 = np.meshgrid(np.arange(start = X_set[:, 0].min() - 1, stop = X_set[:, 0].max() + 1, step = 0.01),
                     np.arange(start = X_set[:, 1].min() - 1, stop = X_set[:, 1].max() + 1, step = 0.01))
plt.contourf(X1, X2, classifier.predict(np.array([X1.ravel(), X2.ravel()]).T).reshape(X1.shape),
             alpha = 0.75, cmap = ListedColormap(('red', 'green')))
plt.xlim(X1.min(), X1.max())
plt.ylim(X2.min(), X2.max())
for i, j in enumerate(np.unique(y_set)):
    plt.scatter(X_set[y_set == j, 0], X_set[y_set == j, 1],
                c = ListedColormap(('red', 'green'))(i), label = j)
plt.title('Random Forest Classification (Test set)')
plt.xlabel('Age')
plt.ylabel('Estimated Salary')
plt.legend()
plt.show()
"""
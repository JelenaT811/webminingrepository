# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 14:26:54 2020

@author: tosic
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 12:40:49 2020

@author: tosic
"""

import numpy as np # for math
import matplotlib.pyplot as plt #visualising data
import pandas as pd # for managing datasers

#Import ds and specify folder where it is
dataset = pd.read_csv('Urls_Extracted.csv')
#Analasys of specific columns:
#result
length = len(dataset)
info = dataset.info()

#str_noref = ["No Referrer"]
#dataset = [~dataset["Referrers"].str.contains('|'.join(str_noref))]
test_dataset = dataset[dataset["Referrers"]=='No Referrer']
test_dataset.drop('Unnamed: 0',axis='columns', inplace=True)
test_dataset['Referrers'] = test_dataset['Referrers'].replace(['No Referrer'],'')
test_dataset.to_csv(path_or_buf = "Testset.csv")

dataset = dataset[dataset["Referrers"]!='No Referrer']
dataset = dataset.dropna()
dataset.drop('Unnamed: 0',axis='columns', inplace=True)
length = len(dataset)
info = dataset.info()
dataset.to_csv(path_or_buf = "Trainingset.csv")

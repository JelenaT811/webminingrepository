# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 12:30:54 2020

@author: tosic
"""

import numpy as np # for math
import matplotlib.pyplot as plt #visualising data
import pandas as pd # for managing datasers

#Import ds and specify folder where it is
dataset = pd.read_csv('Urls_Extracted.csv')

#result
length = len(dataset)
info = dataset.info()

test_dataset = dataset[dataset["Referrers"]=='No Referrer']
test_dataset.drop('Unnamed: 0',axis='columns', inplace=True)
test_dataset['Referrers'] = test_dataset['Referrers'].replace(['No Referrer'],'')
test_dataset.to_csv(path_or_buf = "Testset.csv")

dataset = pd.read_csv('Urls_Extracted.csv')
dataset = dataset[dataset["Referrers"]!='No Referrer']
dataset = dataset.dropna()
dataset.drop('Unnamed: 0',axis='columns', inplace=True)
length = len(dataset)
info = dataset.info()
dataset.to_csv(path_or_buf = "Trainingset.csv")
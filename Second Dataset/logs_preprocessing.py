# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 11:51:36 2020

@author: tosic
"""

import numpy as np # for math
import matplotlib.pyplot as plt #visualising data
import pandas as pd # for managing datasers


#Import ds and specify folder where it is
dataset = pd.read_csv('WebLogs.csv')

#Analasys of specific columns:
    
hosts = dataset["Host"].value_counts()
dates = dataset["Date"].value_counts()
files = dataset["Page/File"].value_counts()
url_param = dataset["URL parameter"].value_counts()
codes = dataset["Response Code"].value_counts()
method = dataset["Request Method"].value_counts()
referrers = dataset["Referrers"].value_counts()
user_agents = dataset["User Agent"].value_counts()

#EMPTY are:
    
users = dataset["User"].value_counts()
v_domain = dataset["Virtual Domain"].value_counts()
cookies = dataset["Cookie"].value_counts()
bndwt = dataset["Bandwidth"].value_counts()

#dropping emty cols

dataset.drop('User',axis='columns', inplace=True)
dataset.drop('Virtual Domain',axis='columns', inplace=True)
dataset.drop('Cookie',axis='columns', inplace=True)
dataset.drop('URL parameter',axis='columns', inplace=True)
dataset.drop('Bandwidth',axis='columns', inplace=True)
#droping rows if colum User Agent is empty

dataset = dataset.dropna(axis=0, subset=['User Agent'])

#Cleaning ResponseCodes (!200) and extensions from Requests
response_str = ['200 - OK','204 - No Content', '206 - Partial Content']
dataset = dataset[dataset["Response Code"].str.contains('|'.join(response_str))]

#Cleaning extensions

searchfor = ['.png', '.jpg','.gif','.css','.jpeg','.cgi','.bot','.mp3','.js','.ico','.swf','.tif','.bmp']
dataset = dataset[~dataset["Page/File"].str.contains('|'.join(searchfor))]
dataset = dataset[~dataset["Referrers"].str.contains('|'.join(searchfor))]

#Cleaning logs with no Referrer and no Page/File data

dataset = dataset.drop(dataset[(dataset['Referrers'] == 'No Referrer') & (dataset['Page/File'] == '/')].index)

#Cleaning Robot requests

search_robottxt = ['robots.txt']
search_agents = ['Googlebot',' AdsBot','Bingbot','BingPreview','facebookexternalhit','Slurp','spider','sogou','proximic','Baiduspider','DuckDuckBot','Yandexbot','ia_archiver','crawl','kraw']

dataset = dataset[~dataset["Page/File"].str.contains('|'.join(search_robottxt))]
dataset = dataset[~dataset["User Agent"].str.contains('|'.join(search_agents))]

dataset = dataset.drop(dataset[(dataset['Referrers'] == 'No Referrer') & (dataset['Request Method'] == 'HEAD')].index)

#droping ALL empty values
dataset = dataset.dropna()
#Saving to .csv
dataset = dataset.sort_values(by = 'Date',ascending = True) 
columns = ["Host","Date","Page/File","Bandwidth","Response Code","Request Method","Referrers","User Agent"]
dataset.to_csv(path_or_buf ="Logs_Preprocessed.csv")
#result
length = len(dataset)
info = dataset.info()
describe = dataset.describe()